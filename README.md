1. Create new folder with env_name
2. Switch to new env folder
3. Create below file
#cat ec2_input.tf
provider "aws" {
  region = "us-east-1"
}
module "ec2-module" {
  source ="/../modules/ec2"
  ami_id = "ami-XXXXXXXXXXXXXXX"
  instancetype = "t2.micro"
  subnetid = "subnet-XXXXXXXXXXXXXX"
  sshkey = "key-XXXXXXXXX"
  resorce_name ="INSTANCE NAME"
  env_name = "ENVIRONMENT"
  product_name = "PRODUCTNAME"
}

4. Update the require details
5. git add .
6. git commit
7. git push
