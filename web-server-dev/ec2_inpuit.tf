provider "aws" {
  region = "us-east-1"
}
module "ec2-module" {
  source ="../modules/ec2"
  ami_id = "ami-0d5eff06f840b45e9"
  instancetype = "t2.small"
  subnetid = "subnet-59882257"
  sshkey = "key-anand-02062021"
  resorce_name ="web-server-1"
  env_name = "dev"
  product_name = "webapp"
}
