variable "ami_id" {
  type  = string
}

variable "instancetype" {
  type = string
}

variable "subnetid" {
  type = string
}

variable "sshkey" {
  type = string
}

variable "resorce_name" {
  type = string
}

variable "env_name" {
  type = string
}

variable "product_name" {
  type = string
}
