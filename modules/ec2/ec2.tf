resource "aws_instance" "web-server-1" {
  ami           = var.ami_id
  instance_type = var.instancetype
  subnet_id     = var.subnetid
  key_name      = var.sshkey
  tags = {
    Name    = var.resorce_name
    Env     = var.env_name
    Product = var.product_name
  }
}
